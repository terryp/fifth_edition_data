from ruamel.yaml import YAML
from rich.prompt import Prompt
from rich import print

yaml = YAML()

with open('./attributes.yaml') as file:
    attrs_dict = yaml.load(file)

all_attrs = list(attrs_dict.keys()) + [attr.title() for attr in attrs_dict]

all_skills = []

for attr in (attr['display']['en'] for attr in attrs_dict.values()):
    print(f'Switching to [italic green]{attr}[/]')
    answer = Prompt.ask(f'What is the next [italic green]{attr}[/] skill?')
    while answer:
        all_skills.append(dict(
            name=answer.lower().replace(' ', '_'),
            attribute=attr,
            display=dict(en=answer),
        ))
        answer = Prompt.ask(f'What is the next [italic green]{attr}[/] skill?')

final_dict = {
    skill['name']: skill
    for skill in all_skills
}

yaml.default_flow_style = False
yaml.indent(mapping=4, sequence=4, offset=0)

with open('./skills.yaml', mode='w') as file:
    yaml.dump(final_dict, file)
