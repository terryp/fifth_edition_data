from ruamel.yaml import YAML
from json import dump
from pathlib import Path
from os import environ
from shutil import copy, copyfileobj, rmtree

yaml = YAML()
SRC = Path(environ['SRC'])
out = SRC/'public'
try:
    out.mkdir()
except FileExistsError:
    rmtree(out)
    out.mkdir()

preamble_file = SRC/'license_preamble.txt'

with preamble_file.open() as file:
    preamble_text = file.read().strip()

all_files = []
for data_file in SRC.glob('*.yaml'):
    all_files.append(data_file.stem)
    out_file = out/data_file.name
    with open(data_file) as file, out_file.open('x') as out_obj:
        data = yaml.load(file)
        file.seek(0)
        out_obj.write(f'# {preamble_text}\n')
        copyfileobj(file, out_obj)

    data['LICENSE'] = preamble_text

    out_json = out_file.with_suffix('.json')
    with out_json.open('x') as file:
        dump(data, file, sort_keys=True)

license_file = SRC/'LICENSE.txt'
copy(license_file, out)

index_path = out/'index.json'

with index_path.open('x') as index_file:
    dump(all_files, index_file)
